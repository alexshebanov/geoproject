/**
 * Created by alexander on 25.05.17.
 */
class Application {
    PlaceDelegate placeService

    Application(PlaceDelegate placeService) {
        this.placeService = placeService
    }

    def getResult() {
        return placeService.getResult()
    }
}
