import groovy.json.JsonSlurper

/**
 * Created by alexander on 25.05.17.
 */
class Request {
    RequestString requestString

    Request(requestString) {
        this.requestString = requestString
    }

    def result() {
        def content = requestString.result.toURL().getText()
        def result = new JsonSlurper().parseText(content)
        return result
    }


}
