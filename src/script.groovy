
int placesCount
if (args.length > 1) {
    placesCount = args[1].toInteger()
} else placesCount = 1


PlaceDelegate placeDelegate = new PlaceDelegate(args[0], placesCount)
Application application = new Application(placeDelegate)
def result = application.getResult()
