/**
 * Created by alexander on 25.05.17.
 */
class PlaceDelegate {
    private PlaceLookUp lookUpService
    private PlaceService placeService
    private def geoLocation
    private int placesCount

    PlaceDelegate(geoLocation, placesCount) {
        lookUpService = new PlaceLookUp()
        this.geoLocation = geoLocation
        this.placesCount = placesCount
    }

    def getResult() {
        placeService = lookUpService.getPlaceService(geoLocation, placesCount)
        return placeService.getResult()
    }
}
