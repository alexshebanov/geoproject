/**
 * Created by alexander on 25.05.17.
 */
class RequestString {
    private def geoLocation
    private def result

    RequestString(geoLocation) {
        this.geoLocation = geoLocation
        Properties properties = new Properties()
        properties.load(new FileInputStream(new File('/home/alexander/IdeaProjects/test/src/PlacesAPI.properties')))
        def APIKey = properties.key
        result = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' +
                this.geoLocation +
                '&rankby=distance&key=' + properties.key
    }
}
