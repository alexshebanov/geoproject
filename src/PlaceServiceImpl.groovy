import groovy.json.JsonOutput

/**
 * Created by alexander on 25.05.17.
 */
class PlaceServiceImpl implements PlaceService {
    Request request
    def placesCount

    PlaceServiceImpl(geoLocation, placesCount) {
        request = new Request(new RequestString(geoLocation))
        this.placesCount = placesCount
    }

    @Override
    def getResult() {
        def json = request.result()
        def status = json.status
        def result
        switch (status) {
            case "ZERO_RESULTS":
                result = new Result(status, null)
                break
            case "OK":
                def placesList = []
                for (int i = 0; i < placesCount; i++) {
                    def name = json.results[i].name + '. ' + json.results[i].vicinity
                    placesList.add(name)
                }
                result = new Result(status, placesList)
                break
            case "INVALID_REQUEST":
                result = new Result(status, null)
                break
        }

        def output = new JsonOutput().toJson(result)
        return output
    }


}
