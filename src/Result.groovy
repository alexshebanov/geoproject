/**
 * Created by alexander on 25.05.17.
 */
class Result {
    final def status
    final def places = []

    Result(status, places) {
        this.status = status
        this.places = places
    }
}
